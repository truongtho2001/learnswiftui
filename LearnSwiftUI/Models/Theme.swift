//
//  Theme.swift
//  LearnSwiftUI
//
//  Created by rta on 12/13/23.
//

import SwiftUI


enum Theme: String {
    case bubblegum
    case buttercup
    case indigo
    case lavender
    case magenta
    case navy
    case orange
    case oxblood
    case periwinkle
    case poppy
    case purple
    case seafoam
    case sky
    case tan
    case teal
    case yellow
    
    var accentColor: Color {
        switch self {
        case .bubblegum, .buttercup, .lavender, .orange, .periwinkle, .seafoam, .sky, .tan, .teal: return .black
        case .indigo, .magenta, .oxblood, .purple: return .white
        case .yellow : return .yellow
        case .navy: return .red
        case .poppy: return .blue
        }
    }
    var mainColor: Color {
        Color(rawValue)
    }
    var name: String {
        rawValue.capitalized
    }
}
