//
//  ScrumDetailView.swift
//  LearnSwiftUI
//
//  Created by rta on 12/14/23.
//

import Foundation
import SwiftUI

struct ScrumDetailView: View {
    let scrum: DailyScrum
    var body: some View {
        List{
            Section(header: Text("Meeting Info")) {
                NavigationLink(destination: MettingView()) {
                    Label("Start Meeting", systemImage: "timer")
                        .font(.headline)
                        .foregroundColor(.accentColor)
                }
                HStack {
                    Label("Lenght",systemImage: "clock")
                    Spacer()
                    Text("\(scrum.lengthInMinutes) minutes")
                }
                .accessibilityElement(children: .combine)
                HStack {
                    Label("Theme", systemImage: "paintpalette")
                    Spacer()
                    Text(scrum.theme.name)
                        .padding(4)
                        .foregroundColor(scrum.theme.accentColor)
                        .background(scrum.theme.mainColor)
                        .cornerRadius(4)
                }
            }
            Section(header: Text("Attendees")) {
                          ForEach(scrum.attendees) { attendee in
                              Label(attendee.name, systemImage: "person")
                          }
                      }

        }
    
    }
}


#Preview {
    ScrumDetailView(scrum: DailyScrum.sampleData[0])
}
