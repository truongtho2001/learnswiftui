//
//  ContentView.swift
//  LearnSwiftUI
//
//  Created by rta on 12/12/23.
//

import SwiftUI

struct MettingView: View {
    var body: some View {
        VStack {
            ProgressView(value: 5,total: 15)
            HStack{
                VStack(alignment:.leading){
                    Text("Seconds Elapsed").font(.caption)
                    Label("300", systemImage:"hourglass.tophalf.fill" )
                    
                }
                Spacer()
                VStack(alignment: .trailing){
                    Text("Seconds Remaining").font(.caption)
                    Label("600", systemImage: "hourglass.tophalf.fill")
                }
                
            }
            .accessibilityElement(children: /*@START_MENU_TOKEN@*/.ignore/*@END_MENU_TOKEN@*/)
            .accessibilityLabel("Time remaining")
            .accessibilityValue("10 minutes")
            Circle()
                .strokeBorder(lineWidth: 24)
            HStack{
                Text("Speaker 1 of 3")
                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                    Image(systemName: "forward.fill")
                })
                .accessibilityLabel("Next speaker")
            }
        }
        .padding(10)
       
    }
}

//#Preview {
//    MettingView()
//}
