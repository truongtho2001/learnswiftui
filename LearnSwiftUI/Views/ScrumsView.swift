//
//  ScrumsView.swift
//  LearnSwiftUI
//
//  Created by rta on 12/14/23.
//

import Foundation
import SwiftUI

struct ScrumsView: View {
    let scrums: [DailyScrum]
    var body: some View {
        NavigationStack {
            List(scrums){scrum in
                NavigationLink(destination: ScrumDetailView(scrum: scrum)) {
                    CardView(scrum: scrum)
                }
                .listRowBackground(scrum.theme.accentColor)
            }
            .navigationTitle("Daily Scrums")
            .toolbar{
                Button(action: {}){
                    Image(systemName: "plus")
                }
            .accessibilityLabel ("New Scrumm")
            }
        }
    }
}

#Preview{
    ScrumsView(scrums: DailyScrum.sampleData)
}
